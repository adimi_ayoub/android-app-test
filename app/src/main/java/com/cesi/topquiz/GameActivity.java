package com.cesi.topquiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cesi.model.Question;
import com.cesi.model.QuestionBank;

import java.util.Arrays;

public class GameActivity extends AppCompatActivity {
    private QuestionBank questionBank;
    private TextView textView;
    private Button[] buttonTab = new Button[4];
    private Question currentQuestion;
    private int score=0;
    public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";
    private final GameActivity context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        questionBank = generateQuestions();
        textView = findViewById(R.id.idTextView);
        buttonTab[0] = findViewById(R.id.button0);
        buttonTab[1] = findViewById(R.id.button1);
        buttonTab[2] = findViewById(R.id.button2);
        buttonTab[3] = findViewById(R.id.button3);
        displayQuestion();
    }

    private QuestionBank generateQuestions()
    {
        Question question1 = new Question("Who is the creator of Android?",
                 Arrays.asList("Andy Rubin","Steve Wozniak","Jake Wharton","Paul Smith"),
                0);

        Question question2 = new Question("When did the first man land on the moon?",
                 Arrays.asList("1958",
                        "1962",
                        "1967",
                        "1969"),
                3);

        Question question3 = new Question("What is the house number of The Simpsons?",
                Arrays.asList("42",
                        "101",
                        "666",
                        "742"),
                3);

        Question question4 = new Question("Who won the most Tour the France?",
                Arrays.asList("Lance Armstrong",
                        "Peter Sagan",
                        "Eddy Mercx",
                        "Laurent Jalabert"),
                3);

        return new QuestionBank(Arrays.asList(question1,
                question2,
                question3,question4));
    }

    private void displayQuestion()
    {
        currentQuestion = questionBank.getQuestion();
        textView.setText(currentQuestion.getQuestion());
        for ( int i=0; i < buttonTab.length ; i ++ ) {
            buttonTab[i].setText(currentQuestion.getChoiceList().get(i));
        }

    }
    public void onAnswer(View view)
    {
        Log.i("======GameActivity", "Tag " + view.getTag());
        int rep = Integer.parseInt ((String)view.getTag() );
        if (rep == currentQuestion.getAnswerIndex())
        {
            // réponse correcte
            Toast.makeText(this, "Correct !", Toast.LENGTH_SHORT).show();
            score ++;
        }
        else
        {
            // mauvaise réponse
            Toast.makeText(this, "Incorrect !", Toast.LENGTH_SHORT).show();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // If this is the last question, ends the game.
                // Else, display the next question.
                if (questionBank.remainingQuestion() > 0)
                    displayQuestion();
                else {
                    // jeu terminé, afficher le score
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Parfait !")
                            .setMessage("Votre score est " + score)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.putExtra(BUNDLE_EXTRA_SCORE, score);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            })
                            .create()
                            .show();
                }
            }
        }, 2000); // LENGTH_SHORT is usually 2 second long

    }
}