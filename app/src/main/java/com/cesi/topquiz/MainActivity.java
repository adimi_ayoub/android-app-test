package com.cesi.topquiz;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cesi.model.User;

public class MainActivity extends AppCompatActivity {
    public final int ACTIVITY_CODE = 109;
    private Button mPlaybutton;
    private EditText mEditText;
    private User user;
    private SharedPreferences preferences;
    public final String FIRSTNAME = "firstname";
    public final String SCORE = "score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPlaybutton = findViewById(R.id.idButton);
        mEditText = findViewById(R.id.idEditPrenom);
        mPlaybutton.setEnabled(false);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.i("======Page1=====","onTextChanged, start:" + start
                //+ "CharSequence:" + s.toString());
                /*if (s.toString().length() != 0)
                    mPlaybutton.setEnabled(true);
                else
                    mPlaybutton.setEnabled(false);*/
                mPlaybutton.setEnabled(s.toString().length() != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.i("======Page1=====","afterTextChanged");
            }
        });
        // regarder les preferences firstname et score
        preferences = getPreferences(MODE_PRIVATE);
        String firstname = preferences.getString(FIRSTNAME, null);
        if (firstname != null)
        {
            mEditText.setText(firstname);
            int score = preferences.getInt(SCORE,-1);
            if (score >= 0)
            {
                TextView text = findViewById(R.id.idTextView);
                text.setText("De retour " + firstname + "! Dernier score : " + score);
            }
        }
    }
    public void onPlay(View view)
    {
        user = new User(mEditText.getText().toString());
        preferences.edit().putString(FIRSTNAME,user.getFirstName()).apply();
        Intent gameActivity = new Intent(MainActivity.this, GameActivity.class);
        //startActivity(gameActivity);
        startActivityForResult(gameActivity,ACTIVITY_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_CODE && resultCode == RESULT_OK)
        {
            int score = data.getIntExtra(GameActivity.BUNDLE_EXTRA_SCORE, 0);
            user.setScore(score);
            Log.i("main","Le score est " + score);
            TextView text = findViewById(R.id.idTextView);

            text.setText(getText(R.string.txtScore).toString() + score);
            preferences.edit().putInt(SCORE,user.getScore()).apply();
        }
    }
}
